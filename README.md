SOMETIME I LIKE BUTTONS ... :)

What do we need for a propper 3d print preperation?
Maybe we'd like change the filament, re-level or preheat the bed, ...
How we achieve this at present? 
Gambling around with the base UI on the Box or clicking a web-gui?
In my opinion some button should be nice as a shortcut to some base functions.

octoprint_rc.py based on REST and the REST API of octoprint.
This and the fact octoprint running on raspberry pi, it is possible to control a 3d printer
via rpi io, any HIT devices and via remote REST messages, without changing the protocol.
So we can use push-buttons, any game-controllers or something like an esp32 with REST Client.
I decided to use a simple HID Gamepad which shipped with retropi (and I already got this)

... btw. this Script ist not a final Solution, it's just a working demo
tailored to my requirements and my creality cr-10s. Feel free to change anything. 
Also I never wrote a Python Script before so it's not professinal coded.

API Link:
http://docs.octoprint.org/en/master/api/index.html

raspbian preperation:
apt install python-requests python-pip
pip install evdev

/ octoprint_rc.py -> remote control script
/ read_rpi_io.py -> raspberry pi io reader, mybe you want to start the script with a "Service Button"
/ test-scripts / ... -> some demo scripts for anaysis

My Button Setup:
start -> run init, zero axes
select -> free axes
D-Pad Arrows -> move X, Y (25mm)
X, B -> move Z (25mm)
A -> toggle heat bed (60C), also switch off heat tool while heated
Y -> leveling next step
R, L -> feed (50mm), unfeed (100mm) filament while the tool is heated, otherwise heat tool to 185C

Have Fun :)
 
 