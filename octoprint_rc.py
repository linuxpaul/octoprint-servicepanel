#!/usr/bin/python
import evdev
import json
import requests

device = evdev.InputDevice("/dev/input/event0")
print(device)

headers = {'X-Api-Key': '5bdb5b3fd2a9b96c6e0a29b456f1715d', 'Content-Type': 'application/json'}

a_zero = 0
l_step = 0
heat_all = 0

mbutton = {"A":289,"B":290,"X":288,"Y":291,"L":292,"R":293,"select":296,"start":297}

def init_axes():
  global a_zero
  global l_step
  url = 'http://localhost/api/printer/command'
  data = {"command" : "G28"}
  response = requests.post(url,json = data,headers = headers)
  a_zero = 1
  l_step = 4

def free_axes():
  global a_zero
  global l_step
  url = 'http://localhost/api/printer/command'
  data = {"command" : "M84"}
  response = requests.post(url,json = data,headers = headers)
  a_zero = 0

def emerg():
  global a_zero
  global l_step
  url = 'http://localhost/api/printer/command'
  data = {"command" : "M112"}
  response = requests.post(url,json = data,headers = headers)
  a_zero = 0

def step_leveling():
  global l_step
  url = 'http://localhost/api/printer/command'
  if a_zero == 0:
    init_axes()

  if l_step == 0:
    data = {"commands" : ["G1 Z5 F500", "G1 X30 Y30 F3000", "G1 Z0.1 F500"]}
    response = requests.post(url,json = data,headers = headers)
    l_step += 1

  elif l_step == 1:
    data = {"commands" : ["G1 Z5 F500", "G1 X30 Y270 F3000", "G1 Z0.1 F500"]}
    response = requests.post(url,json = data,headers = headers)
    l_step += 1

  elif l_step == 2:
    data = {"commands" : ["G1 Z5 F500", "G1 X270 Y270 F3000", "G1 Z0.1 F500"]}
    response = requests.post(url,json = data,headers = headers)
    l_step += 1

  elif l_step == 3:
    data = {"commands" : ["G1 Z5 F500", "G1 X270 Y30 F3000", "G1 Z0.1 F500"]}
    response = requests.post(url,json = data,headers = headers)
    l_step += 1

  elif l_step == 4:
    data = {"commands" : ["G1 Z5 F500", "G1 X150 Y150 F3000", "G1 Z0.1 F500"]}
    response = requests.post(url,json = data,headers = headers)
    l_step = 0


def x_move(direction):
  url = 'http://localhost/api/printer/printhead'
  if direction == 'right':
    data = {"command" : "jog", "x" : 25, "speed" : 2000}
  else:
    data = {"command" : "jog", "x" : -25, "speed" : 2000}
  response = requests.post(url,json = data,headers = headers)

def y_move(direction):
  url = 'http://localhost/api/printer/printhead'
  if direction == 'down':
    data = {"command" : "jog", "y" : 25, "speed" : 2000}
  else:
    data = {"command" : "jog", "y" : -25, "speed" : 2000}
  response = requests.post(url,json = data,headers = headers)

def z_move(direction):
  url = 'http://localhost/api/printer/printhead'
  if direction == 'up':
    data = {"command" : "jog", "z" : 25}
  else: 
    data = {"command" : "jog", "z" : -25}
  response = requests.post(url,json = data,headers = headers)

def heat_nozzle(s):
  url = 'http://localhost/api/printer/tool'
  if s == 'on' and tt_temp < 220:
    data = {"command" : "target", "targets": {"tool0" : 220} }
  elif s == 'off':
    data = {"command" : "target", "targets": {"tool0" : 0} }
  else:
    print 'leave'
    return
  response = requests.post(url,json = data,headers = headers)
 
def heat_bed(s):
  url = 'http://localhost/api/printer/bed'
  if s == 'on' and bt_temp < 60:
    data = {"command" : "target", "target": 60 }
  elif s == 'off':
    data = {"command" : "target", "target" : 0 }
  else:
    return
  response = requests.post(url,json = data,headers = headers)

def fil_move(d):
  if tc_temp < 200:
    heat_nozzle('on')
    return
  url = 'http://localhost/api/printer/command'
  data = {"commands" : ["G92 E0"]}
  response = requests.post(url,json = data,headers = headers)
  url = 'http://localhost/api/printer/tool'
  data = {"command" : "select", "tool" : "tool0"}
  response = requests.post(url,json = data,headers = headers)
  if d == 'feed':
    data = {"command" : "extrude", "amount" : 25}
  else:
    data = {"command" : "extrude", "amount" : -25}
  response = requests.post(url,json = data,headers = headers)

def heat_toggle():
  global heat_all
  if heat_all == 0:
#    heat_nozzle('on')
    heat_bed('on')
    heat_all = 1
  else:
    heat_nozzle('off')
    heat_bed('off')
    heat_all = 0

def p_cancel():
  url = 'http://localhost/api/job'
  data = {"command" : "cancel"}
  response = requests.post(url,json = data,headers = headers)

def p_pause():
  url = 'http://localhost/api/job'
  data = {"command" : "pause", "action": "pause"} 
  response = requests.post(url,json = data,headers = headers)

def p_restart():
  url = 'http://localhost/api/job'
  data = {"command" : "pause", "action": "resume"} 
  response = requests.post(url,json = data,headers = headers)

for event in device.read_loop(): 
  global tc_temp
  global tt_temp
  global bc_temp
  global bt_temp
  url = 'http://localhost/api/printer'
  response = requests.get(url,headers = headers)
  jdata = json.loads(response.text)
  tc_temp = int(jdata['temperature']['tool0']['actual'])
  tt_temp = int(jdata['temperature']['tool0']['target'])
  bc_temp = int(jdata['temperature']['bed']['actual'])
  bt_temp = int(jdata['temperature']['bed']['target'])
  if jdata['state']['flags']['printing'] == True:
    op_mode = 'run'
  elif jdata['state']['flags']['paused'] == True:
    op_mode = 'paused'
  else:
    op_mode = 'ready'

  data = evdev.categorize(event)
  if op_mode == 'run':
    if event.type == evdev.ecodes.EV_KEY:
      pressed = data.keystate
      button = data.scancode
      if button == mbutton["A"] and pressed == 1:
        p_cancel()
      elif button == mbutton["B"] and pressed == 1:
        p_pause()
      elif button == mbutton["select"] and pressed == 1:
        emerg()
    
  elif op_mode == 'paused':
    if event.type == evdev.ecodes.EV_ABS:
      if evdev.ecodes.bytype[data.event.type][data.event.code] == "ABS_X":
        if data.event.value == 0:
	      x_move('left')
        elif data.event.value == 255:
          x_move('right')
      elif event.type == evdev.ecodes.EV_ABS and evdev.ecodes.bytype[data.event.type][data.event.code] == "ABS_Y":
        if data.event.value == 0:
          y_move('up')
        elif data.event.value == 255:
          y_move('down')
    if event.type == evdev.ecodes.EV_KEY:
      pressed = data.keystate
      button = data.scancode
      if button == mbutton["X"] and pressed == 1:
        z_move('up')
      elif button == mbutton["B"] and pressed == 1:
        z_move('down')
      elif button == mbutton["L"] and pressed == 1:
        fil_move('out')
      elif button == mbutton["R"] and pressed == 1:
        fil_move('feed')
      elif button == mbutton["Y"] and pressed == 1:
        p_restart()
      elif button == mbutton["A"] and pressed == 1:
        p_cancel()

  elif op_mode == 'ready':
    if event.type == evdev.ecodes.EV_ABS:
      if evdev.ecodes.bytype[data.event.type][data.event.code] == "ABS_X":
        if data.event.value == 0:
	      x_move('left')
        elif data.event.value == 255:
          x_move('right')
      elif evdev.ecodes.bytype[data.event.type][data.event.code] == "ABS_Y":
        if data.event.value == 0:
          y_move('up')
        elif data.event.value == 255:
          y_move('down')
    elif event.type == evdev.ecodes.EV_KEY:
      pressed = data.keystate
      button = data.scancode
      if button == mbutton["X"] and pressed == 1:
        z_move('up')
      elif button == mbutton["B"] and pressed == 1:
        z_move('down')
      elif button == mbutton["A"] and pressed == 1:
        heat_toggle()
      elif button == mbutton["L"] and pressed == 1:
        fil_move('out')
      elif button == mbutton["R"]and pressed == 1:
        fil_move('feed')
      elif button == mbutton["select"] and pressed == 1:
        free_axes()
      elif button == mbutton["start"] and pressed == 1:
        init_axes()
      elif button == mbutton["Y"] and pressed == 1:
        step_leveling()
