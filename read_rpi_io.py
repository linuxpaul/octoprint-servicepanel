#!/usr/bin/python

import RPi.GPIO as GPIO
import os

gpio_shutdown_pin=15
gpio_service_pin=21

GPIO.setmode(GPIO.BCM)
GPIO.setup(gpio_shutdown_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(gpio_service_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

rc = False
block = False

while True:
  try:
    sd = GPIO.input(gpio_shutdown_pin)
    if sd == 0 and block == False:
      block = True
      os.system("sudo shutdown -h now")

    sm = GPIO.input(gpio_service_pin)
    if sm == 0 and rc == False:
      rc = True
      os.system("python /home/pi/octoprint_rc.py &")
  except:
    pass
time.sleep(.5)

GPIO.cleanup()
