#!/usr/bin/python

import evdev

device = evdev.InputDevice("/dev/input/event0")
print(device)
for event in device.read_loop():
  if event.type == evdev.ecodes.EV_KEY:
    data = evdev.categorize(event) 
    print('+++ ' + str(data))
    print('--- ' + str(data.scancode) + ' ' + str(data.keystate))
  elif event.type == evdev.ecodes.EV_ABS:
    data = evdev.categorize(event)
    print evdev.ecodes.bytype[data.event.type][data.event.code], data.event.value
