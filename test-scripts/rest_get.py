#!/usr/bin/python
import json
import requests

headers = {'X-Api-Key': '<some generated API-Key>', 'Content-Type': 'application/json'}
url = 'http://localhost/api/printer'
response = requests.get(url,headers = headers)

data = json.loads(response.text)

print data['temperature']['tool0']['actual'] 
print data['temperature']['tool0']['target']
